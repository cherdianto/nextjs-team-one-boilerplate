import firebase from "firebase";
// import firebase from "firebase/app";
import "firebase/auth";
import 'firebase/firestore'

// LIVE FIREBASE CHALLENGE
const firebaseConfig = {
  apiKey: "AIzaSyBaotjCYlL7-JaRdnKZnfDZP_lYRQQ7Ltc",
  authDomain: "challenge9-team1.firebaseapp.com",
  databaseURL: "https://challenge9-team1-default-rtdb.firebaseio.com",
  projectId: "challenge9-team1",
  storageBucket: "challenge9-team1.appspot.com",
  messagingSenderId: "833752781618",
  appId: "1:833752781618:web:fd78a8b96e1cd5116a3d2b"
};

if (firebase.apps.length === 0) {
  firebase.initializeApp(firebaseConfig);
}

export default firebase
