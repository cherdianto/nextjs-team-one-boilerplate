export default {
  all: () =>
    Promise.resolve([
      {
        id: 1,
        game_name: "Rock, Paper & Scissors",
        game_detail:
          "The familiar game of Rock, Paper, Scissors is played like this: at the same time, two players display one of three symbols: a rock, paper, or scissors. A rock beats scissors, scissors beat paper by cutting it, and paper beats rock by covering it.",
        img:"../assets/images/rockpaperstrategy.png"
      },
      {
        id: 2,
        game_name: "Under Development",
        game_detail:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id libero vestibulum nunc varius sagittis vitae vel tortor. Praesent blandit nec ante eget luctus. Mauris vestibulum iaculis magna et pretium. Etiam semper sapien sit amet semper finibus.",
        img:"../assets/images/under-construction.png"
      },
      {
        id: 3,
        game_name: "Under Development",
        game_detail:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id libero vestibulum nunc varius sagittis vitae vel tortor. Praesent blandit nec ante eget luctus. Mauris vestibulum iaculis magna et pretium. Etiam semper sapien sit amet semper finibus.",
        img:"../assets/images/under-construction.png"
      },
      {
        id: 4,
        game_name: "Under Development",
        game_detail:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id libero vestibulum nunc varius sagittis vitae vel tortor. Praesent blandit nec ante eget luctus. Mauris vestibulum iaculis magna et pretium. Etiam semper sapien sit amet semper finibus.",
        img:"../assets/images/under-construction.png"
      },
    ]),
};
