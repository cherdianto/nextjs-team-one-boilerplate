import gameListAPI from "../../public/assets/api/gameList";

export const INIT = "gameList/INIT";

export const getAllGameList = () => async (dispatch) => {
  gameListAPI.all().then((gameLists) => {
    return dispatch({
      type: "gameList/INIT",
      payload: gameLists,
    });
  });
};
