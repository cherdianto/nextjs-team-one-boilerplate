export const computerChoice = () => {
  return {
    type: "COMP_CHOICE",
  };
};

export const playerChoice = () => {
  return {
    type: "PLAYER_CHOICE",
  };
};
