import firebase from '../../services/firebase'
import 'firebase/auth'

export const getScore = () => {
    return async (dispatch, getState) => {
        try {
            const snapshot = await firebase.database().ref("users").orderByChild('gameHistory').startAt(0).once('value')
            const allData = snapshot.val();
            const dataUser=[];
            for(let id in allData){
              dataUser.push({username:allData[id].username,
                              score:allData[id].gameHistory[allData[id].gameHistory.length-1][1]})
          }
            dataUser.sort(function (a, b) {
               return b.score - a.score;
            });
            dispatch({
                type: 'GET_SCORE_SUCCESS',
                payload: dataUser
            })
        } catch (err) {
            dispatch({
                type: 'GET_SCORE_ERROR',
                err
            })
        }
    }
}


export const getDetail = (uid) => {
    const userUid = 0
    return async (dispatch, getState) => {
        try {
            const snapshot = await firebase.database().ref(`detailGame/${userUid}`).once('value')
            const detailsGame = snapshot.val(); 
            // console.log("ini data game")
            // console.log(detailsGame)   
            dispatch({
                type: 'GET_DETAIL_SUCCESS',
                payload: detailsGame
            })
        } catch (err) {
            dispatch({
                type: 'GET_DETAIL_ERROR',
                err
            })
        }
    }
}