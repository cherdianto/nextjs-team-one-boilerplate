// import firebase from '../../services/firebase'
// import 'firebase/auth'

// export const signIn = (credentials) => {
//     console.log(credentials)
    
//     return (dispatch, getState) => {
 
//         firebase.auth().signInWithEmailAndPassword(
//             credentials.email,
//             credentials.password
//         ).then((res) => {
//             console.log(res)
//             dispatch({
//                 type: 'LOGIN_SUCCESS'
//             })
//         }).catch((err) => {
//             dispatch({
//                 type: 'LOGIN_ERROR',
//                 err
//             })
//         })

//         console.log('didalem return action')
//     }
// };

// // export const signOut = () => {
// //     return (dispatch, getState, {getFirebase}) => {
// //         const firebase =  getFirebase();

// //         firebase.auth().signOut().then(() => {
// //                 dispatch({
// //                     type: 'SIGNOUT_SUCCESS'
// //             })
// //         });
// //     }
// // };

// // export const signUp = (newUser) => {
// //     return (dispatch, getstate, {getFirebase, getFirestore}) => {
// //         const firebase = getFirebase();
// //         const firestore = getFirestore();

// //         firebase.auth().createUserWithEmailAndPassword(
// //             newUser.email,
// //             newUser.password
// //         ).then((res) => {
// //             return firestore.collection('users').doc(res.user.uid).set({
// //                 firstName: newUser.firstName,
// //                 lastName: newUser.lastName,
// //                 initials: newUser.firstName[0] + newUser.lastName[0]
// //             })
// //         }).then(() => {
// //             dispatch({
// //                 type: 'SIGNUP_SUCCESS'
// //             })
// //         }).catch(err => {
// //             dispatch({
// //                 type: 'SIGNUP_ERROR',
// //                 err
// //             })
// //         })
// //     }
// // }

import firebase from '../../services/firebase'
import 'firebase/auth'

export const signIn = (credentials) => {
    // console.log(credentials)
    
    return (dispatch, getState) => {
 
        firebase.auth().signInWithEmailAndPassword(
            credentials.email,
            credentials.password
        ).then((res) => {
            // console.log(res.user)
            dispatch({
                type: 'LOGIN_SUCCESS',
                payload: res.user
            })
            // pass UID ke then berikutnya
            // console.log('login lanjut ke then')
            // return res.user.uid

        }).catch((err) => {
            dispatch({
                type: 'LOGIN_ERROR',
                err
            })
        })
    }
};

export const signOut = () => {
    return (dispatch, getState) => {

        firebase.auth().signOut().then(() => {
                dispatch({
                    type: 'SIGNOUT_SUCCESS'
            })
        });
    }
};

export const signUp = (newUser) => {
    return (dispatch, getstate) => {
        console.log(newUser)
        let usr;

        firebase.auth().createUserWithEmailAndPassword(
            newUser.email,
            newUser.password
        ).then(async (res) => {
            let userId = res.user.uid;
            usr = res.user;
            console.log(newUser)
            return await firebase.database().ref('users_new/' + userId).set({
                initial: newUser.firstName[0] + newUser.lastName[0],
                firstName: newUser.firstName,
                lastName: newUser.lastName,
                location: newUser.location,
                lastLogin: [Date.now()],
                achievement: "newbie"
            })
        }).then(() => {
            console.log(usr)
            console.log(newUser)
            dispatch({
                type: 'SIGNUP_SUCCESS',
                payload: {
                    initial: newUser.firstName[0] + newUser.lastName[0],
                    firstName: newUser.firstName,
                    lastName: newUser.lastName,
                    location: newUser.location,
                    user: usr 
                }
            })
        }).catch(err => {
            console.log('dispatch err')
            dispatch({
                type: 'SIGNUP_ERROR',
                err
            })
        })
    }
}

export const getUserData = (uid) => {
    const userUid = uid
    return async (dispatch, getState) => {
        try {
            const snapshot = await firebase.database().ref(`users_new/${userUid}`).once('value')
            const userData = snapshot.val();
            
            console.log('user data:')
            console.log(userData)
    
            dispatch({
                type: 'GET_USER_DATA_SUCCESS',
                payload: userData
            })
        } catch (err) {
            dispatch({
                type: 'GET_USER_DATA_ERROR',
                err
            })
        }
    }
}

export const updateProfile = (profile) => {
    const updates = {
        firstName: profile.firstName,
        lastName: profile.lastName,
        location: profile.location
    }
    return async (dispatch, getState) => {
        try {
            await firebase.database().ref(`users_new/${profile.user.uid}`).update(updates)
            console.log('update DB success')
            
            dispatch({
                type: 'UPDATE_PROFILE_SUCCESS',
                payload: profile
            })

          } catch (err) {
            console.log(err.message)
            dispatch({
                type: 'UPDATE_PROFILE_ERROR',
                err
            })
          }
    }
}