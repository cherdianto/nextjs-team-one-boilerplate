import authReducer from './authReducer'
import detailReducer from './detailReducer'
import gameListReducer from "./gameListReducer";
import { combineReducers } from 'redux'

const rootReducer = combineReducers({
    auth: authReducer,
    detail:detailReducer,
    gameList: gameListReducer,
});

export default rootReducer;
