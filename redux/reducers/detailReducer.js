const initState = {
    loading:false,
    detailGame:{
        img:null,
        title: null,
        detail: null,
        link:null
    },
    dataScore: {
        username: null,
        score: null
    }
}

const detailReducer = (state = initState, action) => {
    switch(action.type){
        case 'GET_DETAIL_SUCCESS':
            console.log('get game data success')
            console.log(action.payload)
            return {
                ...state,
                detailGame:{
                    img:action.payload.gambar,
                    title: action.payload.nama,
                    detail: action.payload.isi,
                    link:action.payload.link
                }
            };
        case 'GET_SCORE_SUCCESS':
            console.log('get score data success')
            console.log(action.payload)
            return {
                ...state,
                loading:true,
                dataScore:action.payload
            };
        case 'GET_SCORE_ERROR':
            return state;
        case 'GET_DETAIL_ERROR':
            return state;
        default:
            return state
    }
}

export default detailReducer;