const initState = {
    authError: null,
    user: null,
    profile: {
        initial: null,
        firstName: null,
        lastName: null,
        location: null,
        achievement: null
    }
}

const authReducer = (state = initState, action) => {
    switch(action.type){
        case 'LOGIN_ERROR':
            return {
                ...state,
                authError: 'Login failed'
            }
        case 'LOGIN_SUCCESS':
            console.log('login success')
            // console.log(state)
            return {
                ...state,
                authError: null,
                user: action.payload
            }
        case 'SIGNOUT_SUCCESS':
            console.log('signout success');
            return {
                ...state,
                authError: null,
                user: null
            }
        case 'SIGNUP_SUCCESS':
            console.log('signup success');
            console.log(action.payload)
            return {
                ...state,
                authError: null,
                profile: {
                    firstName: action.payload.firstName,
                    lastName: action.payload.lastName,
                    location: action.payload.location,
                    initial: action.payload.initial,
                    achievement: 'newbie'
                },
                user: action.payload.user
            }
        case 'SIGNUP_ERROR':
            console.log('signup error');
            return {
                ...state,
                authError: action.err.message
            }
        case 'GET_USER_DATA_SUCCESS':
            console.log('get user data success')
            console.log(action.payload)
            console.log(state)
            const newState = {
                ...state,
                profile: {
                    initial: action.payload.initial,
                    firstName: action.payload.firstName,
                    lastName: action.payload.lastName,
                    location: action.payload.location,
                    achievement: action.payload.achievement,
                }
            }
            console.log(newState)
            return newState;

        case 'GET_USER_DATA_ERROR':
            console.log('get user data error')
            console.log(action.err.message)
            return state;
        case 'UPDATE_PROFILE_SUCCESS': 
            console.log('update profile success')
            const newData = {
                ...state,
                profile: {
                    initial: action.payload.initial,
                    firstName: action.payload.firstName,
                    lastName: action.payload.lastName,
                    location: action.payload.location,
                    achievement: action.payload.achievement,
                }
            }
            console.log(newData)
            return newData
        case 'UPDATE_PROFILE_ERROR': 
            console.log('update profile error')
            return state;
        default:
            return state
    }
    
}

export default authReducer;