import { INIT } from "../actions/gameListAction";

const initState = [
  {
    id: "",
    game_name: "",
    game_detail: "",
  },
];

const gameListReducer = (state = initState, action) => {
  switch (action.type) {
    case INIT:
      return action.payload;
    default:
      return state;
  }
};

export default gameListReducer;
