const initState = {
  player: "",
  computer: "",
  winner: "VS",
  classCompPick: "",
  skorPlayer: 0,
  skorComputer: 0,
  onClick: 0,
  gameHistory: null,
  latestScore: null,
};

const gameReducer = (state = initState, action) => {
  switch (action.type) {
    case "PLAYER_CHOICE":
      console.log("pilihan PLAYER terpanggil");
      return state;

    case "COMP_CHOICE":
      console.log("pilihan computer terpanggil");
      return state;

    default:
      return state;
  }
};

export default gameReducer;
