// import { useMemo } from 'react'
// import thunk from 'redux-thunk'
// // import firebase from 'firebase'
// import { createStore, applyMiddleware, compose } from 'redux';
// import rootReducer from '../redux/reducers/rootReducer';
// import { connect, Provider, useSelector } from 'react-redux';
// import { createFirestoreInstance, getFirestore, reduxFirestore } from 'redux-firestore';
// import { ReactReduxFirebaseProvider, getFirebase, isLoaded, useFirebase, reduxFirebase } from 'react-redux-firebase';
// import firebase from '../services/firebase'

// let store;

// const middleware = [
//     thunk
// ]

// function initStore(initialState) {
//     // console.log(state)
//   return createStore(
//     rootReducer,
//     initialState,
//     compose(
//         applyMiddleware(...middleware),
//         reduxFirestore(firebase)
//     )
//   )
// }

// export const initializeStore = (preloadedState) => {
//   let _store = store ?? initStore(preloadedState)

//   // After navigating to a page with an initial Redux state, merge that state
//   // with the current state in the store, and create a new store
//   if (preloadedState && store) {
//     _store = initStore({
//       ...store.getState(),
//       ...preloadedState,
//     })
//     // Reset the current store
//     store = undefined
//   }

//   // For SSG and SSR always create a new store
//   if (typeof window === 'undefined') return _store
//   // Create the store once in the client
//   if (!store) store = _store

//   return _store
// }

// export function useStore(initialState) {
//   const store = useMemo(() => initializeStore(initialState), [initialState])
//   console.log('usestore store.js')
//   console.log(store)
//   return store
// }

import { useMemo } from 'react'
// SIMPLE REDUX
import { createStore, applyMiddleware, compose  } from 'redux'
import rootReducer from './reducers/rootReducer'

//FIREBASE
import firebase from '../services/firebase'
import thunk from 'redux-thunk'

const middleware = [
    thunk
]


// const store = createStore(rootReducer,
//         compose(
//             applyMiddleware(...middleware)
//         )
// )

// export default store


// nyobain hydration
let store;

function initStore(initialState) {
  return createStore(
    rootReducer,
    initialState,
    compose(
        applyMiddleware(...middleware)
    )
  )
}

export const initializeStore = (preloadedState) => {
  let _store = store ?? initStore(preloadedState)

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    _store = initStore({
      ...store.getState(),
      ...preloadedState,
    })
    // Reset the current store
    store = undefined
  }

  // For SSG and SSR always create a new store
  if (typeof window === 'undefined') return _store
  // Create the store once in the client
  if (!store) store = _store

  return _store
}

export function useStore(initialState) {
  const store = useMemo(() => initializeStore(initialState), [initialState])
  return store
}