import React from "react";
import { useEffect } from "react";
import { Card, Col, Row, Container, Button } from "react-bootstrap";
import classes from "../styles/GameList.module.css";
import Link from "next/link";
import Head from "next/head";
import { CMS_NAME } from "../lib/constants";
import { connect, useDispatch, useSelector } from "react-redux";
// import GameListCard from "../components/gameList";
import Layout from "../components/layout";
import { getAllGameList } from "../redux/actions/gameListAction";

const GameList = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllGameList());
  }, [dispatch]);

  return (
    <Layout>
      <Head>
        <title>An Online Game Site by {CMS_NAME}</title>
      </Head>
      <div className={classes.GameList}>
        <Container className="py-5">
          <h1 className="font-weight-bold text-center">
            Check out these EPIC Games!
          </h1>
          <Row className="justify-content-center">
            <Col>
              <div className={classes.GameListCard}>
                <Card
                  className="align-item-center"
                  style={{ width: "26rem" }}
                  bg="dark"
                >
                  {/* <Card.Img variant="top" src={props.img} /> */}
                  <Card.Body>
                    <Card.Title className="text-white">
                      Rock, Paper & Scissors
                    </Card.Title>
                    <Card.Text className="text-white">
                      The familiar game of Rock, Paper, Scissors is played like
                      this: at the same time, two players display one of three
                      symbols: a rock, paper, or scissors. A rock beats
                      scissors, scissors beat paper by cutting it, and paper
                      beats rock by covering it.
                    </Card.Text>
                    <Link href="/game-detail">
                      <Button className="px-4 py-2" variant="danger">
                        Game Details
                      </Button>
                    </Link>
                  </Card.Body>
                </Card>
              </div>
            </Col>
            <Col>
              <div className={classes.GameListCard}>
                <Card
                  className="align-item-center"
                  style={{ width: "26rem" }}
                  bg="dark"
                >
                  {/* <Card.Img variant="top" src={props.img} /> */}
                  <Card.Body>
                    <Card.Title className="text-white">
                      Under Development
                    </Card.Title>
                    <Card.Text className="text-white">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Quisque id libero vestibulum nunc varius sagittis vitae
                      vel tortor. Praesent blandit nec ante eget luctus. Mauris
                      vestibulum iaculis magna et pretium. Etiam semper sapien
                      sit amet semper finibus.
                    </Card.Text>
                    <Link href="/game-detail">
                      <Button className="px-4 py-2" variant="danger">
                        Game Details
                      </Button>
                    </Link>
                  </Card.Body>
                </Card>
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <div className={classes.GameListCard}>
                <Card
                  className="align-item-center"
                  style={{ width: "26rem" }}
                  bg="dark"
                >
                  {/* <Card.Img variant="top" src={props.img} /> */}
                  <Card.Body>
                    <Card.Title className="text-white">
                      Under Development
                    </Card.Title>
                    <Card.Text className="text-white">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Quisque id libero vestibulum nunc varius sagittis vitae
                      vel tortor. Praesent blandit nec ante eget luctus. Mauris
                      vestibulum iaculis magna et pretium. Etiam semper sapien
                      sit amet semper finibus.
                    </Card.Text>
                    <Link href="/game-detail">
                      <Button className="px-4 py-2" variant="danger">
                        Game Details
                      </Button>
                    </Link>
                  </Card.Body>
                </Card>
              </div>
            </Col>
            <Col>
              <div className={classes.GameListCard}>
                <Card
                  className="align-item-center"
                  style={{ width: "26rem" }}
                  bg="dark"
                >
                  {/* <Card.Img variant="top" src={props.img} /> */}
                  <Card.Body>
                    <Card.Title className="text-white">
                      Under Development
                    </Card.Title>
                    <Card.Text className="text-white">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Quisque id libero vestibulum nunc varius sagittis vitae
                      vel tortor. Praesent blandit nec ante eget luctus. Mauris
                      vestibulum iaculis magna et pretium. Etiam semper sapien
                      sit amet semper finibus.
                    </Card.Text>
                    <Link href="/game-detail">
                      <Button className="px-4 py-2" variant="danger">
                        Game Details
                      </Button>
                    </Link>
                  </Card.Body>
                </Card>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </Layout>
  );
};

const mapStateToProps = (state) => {
  console.log(state);
  return {
    authError: state.auth.authError,
    user: state.auth.user,
    profile: state.auth.profile,
  };
};

// const mapDispatchToProps = (e) => {
//   return {
//     getAllGameList: getAllGameList,
//   };
// };
export default connect(mapStateToProps)(GameList);
