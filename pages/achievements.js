import "../styles/Achievments.module.css";
import { CardGroup, Card, Container, Badge, Button } from "react-bootstrap";
import Image from "next/image";
import Link from "next/link";
import styles from "../styles/Achievments.module.css";

const Achievments = () => {
  return (
    <div className={styles.achievments}>
      <div className={styles.header__achievments}>
        <Link href="/"><Button className="mr-5 p-30">
            <a> BACK</a>
          </Button>
        </Link>
        <h1>ACHIEVMENTS</h1>
      </div>
      <Container>
        <h1 className={styles.allecipmen}>Your Achievments</h1>
        <CardGroup>
          <Card className={styles.card1}>
            <Image
              src="/../public/assets/images/newbiebadge.png"
              alt="Badge newbie"
              width={250}
              height={300}
            />
            <Card.Body>
              <Card.Title>CONGRATULATIONS</Card.Title>
              <Card.Text>You have completed 9 games .</Card.Text>
            </Card.Body>
            <Card.Footer ml-left>
              <h1>
                <Badge variant="danger">MISSION COMPLETE</Badge>
              </h1>
            </Card.Footer>
          </Card>
          <Card className={styles.card1}>
            <Image
              src="/../public/assets/images/middle-badge.png"
              alt="Badge newbie"
              width={250}
              height={300}
            />
            <Card.Body>
              <Card.Title>CONGRATULATIONS</Card.Title>
              <Card.Text>You have completed 18 games .</Card.Text>
            </Card.Body>
            <Card.Footer>
              <h1>
                <Badge variant="secondary">NOT COMPLETE</Badge>
              </h1>
            </Card.Footer>
          </Card>
          <Card className={styles.card1}>
            <Image
              src="/../public/assets/images/pro-badge.png"
              alt="Badge newbie"
              width={250}
              height={300}
              padding={20}
            />
            <Card.Body>
              <Card.Title>CONGRATULATION</Card.Title>
              <Card.Text>You have completed 27 games .</Card.Text>
            </Card.Body>
            <Card.Footer>
              <h1>
                <Badge variant="secondary">NOT COMPLETE</Badge>
              </h1>
            </Card.Footer>
          </Card>
        </CardGroup>
      </Container>
    </div>
  );
};

export default Achievments;
