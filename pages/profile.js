import Layout from '../components/layout'
import { connect } from 'react-redux'
import { Container, Table } from 'react-bootstrap'
import classes from '../styles/UpdateProfile.module.css'
import Link from 'next/link'
import Image from 'react-bootstrap/Image'

const Profile = (props) => {
    return ( 
        <Layout>
                 <Container className={classes.container}>
                     <h1 className={`${classes.mobileCenter} mt-5`}>Your Profile</h1>
                     <div  className="row d-flex">
                         <div className={`${classes.mobileCenter} col-sm-md-lg-6`}>
                            <Image className="mx-3 "  src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=400" roundedCircle/>
                         </div>
                         <div className={`${classes.mobileCenter} col-sm-md-lg-6`}>
                            <Table responsive>
                                <tbody>  
                                    <tr>
                                        <td>Firstname</td>
                                        <td>{props.profile.firstName}</td>
                                    </tr>
                                    <tr>
                                        <td>Lastname</td>
                                        <td>{props.profile.lastName}</td>
                                    </tr>
                                    <tr>
                                        <td>Location</td>
                                        <td>{props.profile.location}</td>
                                    </tr>
                                    <tr>
                                        <td>Achievement</td>
                                        <td>{props.profile.achievement}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <Link href="/update-profile" >
                                                <button class="btn btn-primary">Update Profile</button>
                                            </Link>
                                        </td>
                                    </tr>
                                
                                </tbody>
                            </Table>
                         </div> 
                     </div>
                 </Container>
             </Layout>
     );
}
 
const mapStateToProps = (state) => {
    return {
        profile: state.auth.profile,
        user: state.auth.user
    }
}
export default connect(mapStateToProps)(Profile);

