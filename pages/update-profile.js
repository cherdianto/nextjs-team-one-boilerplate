import React, { Component} from 'react'
import classes from '../styles/UpdateProfile.module.css'
import Layout from '../components/layout'
import { Container,  } from 'react-bootstrap'
import { updateProfile } from '../redux/actions/authActions'
import { connect } from 'react-redux'
import Link from 'next/link'
import Image from 'react-bootstrap/Image'

class UserProfile extends Component {
    state = {
            lastName: '',
            firstName: '',
            location: '',
            achievement: '',
            initial: '',
            user : ''
    }

    componentDidMount() {
        const {
            lastName,
            firstName,
            location,
            achievement,
            initial } = this.props.profile

        this.setState({
            lastName,
            firstName,
            location,
            achievement,
            initial,
            user: this.props.user 
        })

        console.log(this.state)
    }

    handleChange = (e) => {
        console.log('onchange', e.target.id)
        this.setState({
            [e.target.id]: e.target.value
                
        })
        console.log(this.state.profile)
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state)
        this.props.updateProfile(this.state);
    }
    render() {          
        console.log(this.state)

        return (
            <Layout>
                <Container className={classes.container}>
                    <h1 className={`${classes.mobileCenter} mt-5`}>Update Profile</h1>
                    <div  className="row d-flex">
                        <div className={`${classes.mobileCenter} col-sm-md-lg-6`}>
                            <Image className="mx-3 "  src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=400" roundedCircle/>
                        </div>
                        <div className={`${classes.mobileCenter} col-sm-md-lg-6`}>
                            <form onSubmit={this.handleSubmit}>
                                <div className="input-field">
                                    <label htmlFor="firstName">First Name</label><br />
                                    <input type="firstName" id="firstName" onChange={this.handleChange} value={this.state.firstName}/>
                                </div>
                                <div className="input-field">
                                    <label htmlFor="lastName">Last Name</label><br />
                                    <input type="lastName" id="lastName" onChange={this.handleChange} value={this.state.lastName}/>
                                </div>
                                <div className="input-field">
                                    <label htmlFor="location">Location</label><br />
                                    <input type="location" id="location" onChange={this.handleChange} value={this.state.location}/>
                                </div>
                                <div className="input-field">
                                    <br />
                                    <button className="btn btn-primary">Update</button>
                                    
                                </div>
                            </form>
                            <br />
                            <Link href="/profile"><button className="btn btn-primary">Back to profile</button></Link>
                        </div> 
                    </div>
                </Container>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.auth.profile,
        user: state.auth.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateProfile: (profile) => dispatch(updateProfile(profile))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile)
