import MainGame from "../components/gameplay/mainGame";

const gamePlay = () => {
  return (
    <div>
      <MainGame />
    </div>
  );
};

export default gamePlay;
