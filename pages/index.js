// import Container from '../components/container'
import { Container, Jumbotron } from 'react-bootstrap'
import Layout from '../components/layout'
import Head from 'next/head'
import { CMS_NAME } from '../lib/constants'
import { connect } from 'react-redux'
import JumbotronComponent from '../components/jumbotronComponent'
import GameList from '../components/gameListSementara'
const Index = (props) => {
  const { authError, user, profile } = props
  return (
    <>
      <Layout>
        <Head>
          <title>An Online Game Site by {CMS_NAME}</title>
        </Head>
          {/* <h3>HOME CONTAINER</h3>
          
          { user ? <h2>Welcome home {profile.firstName}</h2> : <h2>You are not logged in yet, Please login or register</h2> } */}
          <JumbotronComponent/>
          <GameList/>
      </Layout>
    </>
  )
}
const mapStateToProps = (state) => {
  
  return {
    authError: state.auth.authError,
    user: state.auth.user,
    profile: state.auth.profile
  }
}

// const mapDispatchToProps = (dispatch) => {
//   return {
//       signIn: (creds) => dispatch(signIn(creds))
//   }
// }

export default connect(mapStateToProps)(Index)

