import React, { Component } from 'react'
import Layout from '../components/layout'
import { Container } from 'react-bootstrap'
import { connect } from 'react-redux'
import { signIn, getUserData } from '../redux/actions/authActions'
import { Redirect } from 'react-router-dom'
import { withRouter, NextRouter } from 'next/router'
import Router from 'next/router'


class SignIn extends Component {
    state = {
        email: '',
        password: ''
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
        // console.log(this.state)
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.signIn(this.state);
    }

    render() {
        console.log(this.props)
        const { authError, user } = this.props;

        if(user) {
            this.props.getUserData(user.uid)
            Router.push('/')
        }

        return (
            <Layout>
                <Container>
                    <form onSubmit={this.handleSubmit} className="white">
                        <h5>Sign In</h5>
                        <div className="input-field">
                            <label htmlFor="email">Email</label><br />
                            <input type="email" id="email" onChange={this.handleChange}/>
                        </div>
                        <div className="input-field">
                            <label htmlFor="password">Password</label><br />
                            <input type="password" id="password" onChange={this.handleChange}/>
                        </div>
                        <div className="input-field"><br />
                            <button className="btn btn-primary">Login</button>
                            <div className="text-center">
                                { authError ? <p>{authError}</p> : null}
                            </div>
                        </div>
                    </form>  
                </Container>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => {
    console.log(state)
    return {
        authError: state.auth.authError,
        user: state.auth.user
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        signIn: (creds) => dispatch(signIn(creds)),
        getUserData: (uid) => dispatch(getUserData(uid))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SignIn)



