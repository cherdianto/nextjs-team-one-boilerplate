import Layout from '../components/layout'
import {useEffect} from 'react'
import GameDetailComponent from '../components/gameDetailComponent'
import * as ReactBootstrap from 'react-bootstrap'
import { getDetail, getScore } from '../redux/actions/detailAction'

import { connect,useDispatch } from 'react-redux'

function GameDetail(props){
    // const dispatch = useDispatch();
     useEffect(() => {
        // document.querySelector(".navbar").style.backgroundColor = "black";
        // dispatch(getScore())
        props.getDetail(0),
        props.getScore()
       
     }, [])
     
     const { loading} = props;
        return (
            <Layout>
                <div>
                {
                    loading ? <GameDetailComponent/> : <ReactBootstrap.Spinner animation="border"/>
                }

                </div>
            </Layout>
        )
    }

    const mapStateToProps = (state) => {
        return {
            loading: state.detail.loading,
        }
    }
    const mapDispatchToProps = (dispatch) => {
        return {
            getScore: () => dispatch(getScore()),
            getDetail:(uid)=>dispatch(getDetail(uid))
        }
    }

    export default connect(mapStateToProps,mapDispatchToProps)(GameDetail) 