// WITH HYDRATION
import '../styles/index.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import App from 'next/app'
import { Provider} from 'react-redux'
import withRedux from 'next-redux-wrapper'
import React from 'react'
// import store from '../redux/store';
import { useStore } from '../redux/store'
import { useState, useSelector, useEffect } from 'react'

// FIREBASE
import firebase from '../services/firebase'

// let store;

export default function MyApp({ Component, pageProps }) {
  const store = useStore(pageProps.initialReduxState)

  return (
    <Provider store={store}>
        <Component {...pageProps} />
    </Provider>
  )
}

// const makeStore = () => store;

// export default withRedux(makeStore)(MyApp);
