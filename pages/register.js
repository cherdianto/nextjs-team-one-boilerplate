import React, { Component } from 'react'
import Layout from '../components/layout'
import { Container } from 'react-bootstrap'
import { connect } from 'react-redux'
import { signIn, signUp } from '../redux/actions/authActions'
import { Redirect } from 'react-router-dom'
import { withRouter, NextRouter } from 'next/router'
import Router from 'next/router'


class SignUp extends Component {
    state = {
        email: '',
        password: '',
        lastName: '',
        firstName: '',
        location: '',
        initial: ''
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
        console.log(this.state)
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.signUp(this.state);
    }
    render() {
        const { authError, user } = this.props;
        // console.log(auth)

        if(user) {
            Router.push('/')
        }

        return (
            <Layout>
                <Container>
                    <form onSubmit={this.handleSubmit} className="white">
                        <h5>Sign In</h5>
                        <div className="input-field">
                            <label htmlFor="email">Email</label><br />
                            <input type="email" id="email" onChange={this.handleChange}/>
                        </div>
                        <div className="input-field">
                            <label htmlFor="password">Password</label><br />
                            <input type="password" id="password" onChange={this.handleChange}/>
                        </div>
                        <div className="input-field">
                            <label htmlFor="firstName">FirstName</label><br />
                            <input type="firstName" id="firstName" onChange={this.handleChange}/>
                        </div>
                        <div className="input-field">
                            <label htmlFor="lastName">LastName</label><br />
                            <input type="lastName" id="lastName" onChange={this.handleChange}/>
                        </div>
                        <div className="input-field">
                            <label htmlFor="location">Location</label><br />
                            <input type="location" id="location" onChange={this.handleChange}/>
                        </div>
                        <div className="input-field">
                            <button className="btn btn-primary">Login</button>
                            <div className="text-center">
                                { authError ? <p>{authError}</p> : null}
                            </div>
                        </div>
                    </form>  
                </Container>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => {
    // console.log(state)
    return {
        authError: state.auth.authError,
        user: state.auth.user,
        profile: state.auth.profile
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        signUp: (newUser) => dispatch(signUp(newUser))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SignUp)



