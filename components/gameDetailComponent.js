import {Fragment} from 'react'
import Description from './cardDescription'
import { Container,Row,Col,Card,Table } from 'react-bootstrap';
import style from '../styles/GameDetail.module.css'

import { connect} from 'react-redux'

function gameDetailComponent(props) {
    const dataUser = props.score
    const userIds = Object.keys(dataUser)
    // console.log(userIds)
    return (
        <div>
            <Container className="py-5">
                    <Row className={style.row}>
                        <Col className={style.col}>
                            {/* <Description titleGame={this.state.titleGame} bodyGame={this.state.bodyGame} imgGame={this.state.imgGame} linkGame={this.state.linkGame}/> */}
                            <Description/>                            
                        </Col>
                        
                        <Col className={style.col}>
                        <Card className={style.card}>
                            <h1>All User Score</h1>
                            <Table hover responsive>
                            <thead>
                                <tr>
                                <th>Username</th>
                                <th>Score</th>
                                </tr>
                            </thead>
                            <tbody>
                            { 
                                    userIds.map((el, i) => {
                                        const detailData = dataUser[el]
                                        return (
                                            <Fragment key={el}>
                                                <tr>
                                                    <td>{detailData.username}</td>
                                                    <td>{detailData.score}</td>
                                                </tr>
                                            </Fragment>
                                            )
                                    })
                                }
                            </tbody>
                            </Table> 
                        </Card>
                        </Col>
                    </Row>
            </Container>
        </div>
    )
}

const mapStateToProps = (state) => {
    // console.log(state)
    return {
        score: state.detail.dataScore,
    }
}
export default connect(mapStateToProps)(gameDetailComponent) 