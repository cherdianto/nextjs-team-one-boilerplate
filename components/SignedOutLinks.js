import React from 'react';
import { NavLink } from 'react-router-dom';
import { Nav, Navbar } from "react-bootstrap";
import Link from 'next/link'
import classes from '../styles/Navbar.module.css'


function SignedOutLinks(props) {
    return (
        <Nav className={`${classes.navLink} ml-auto`}>
                <Nav.Link><Link href="/register"><a>Register</a></Link></Nav.Link>
                <Nav.Link><Link href="/login"><a>Login</a></Link></Nav.Link>
        </Nav>
    )
}

export default SignedOutLinks;
