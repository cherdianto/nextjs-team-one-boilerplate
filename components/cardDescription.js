import React from 'react'
import {Card, Button} from 'react-bootstrap'
import Link from 'next/link'
import style from '../styles/GameDetail.module.css'
import { connect} from 'react-redux'
function CardGameDescription(props) {
    return (
        <div>
            <Card className={style.card}>
                <h1>{props.detailGame.title}</h1>
                <p>{props.detailGame.detail}</p>
                    <img className="fakeimg" src={props.detailGame.img} style={{width:"100%"}}/>
                <Button variant="dark" className="mt-5"><Link href="/game-play">Play Game</Link></Button>
            </Card>
        </div>
    )
}

const mapStateToProps = (state) => {
    console.log(state)
    return {
        detailGame: state.detail.detailGame
    }
}
export default connect(mapStateToProps)(CardGameDescription) 