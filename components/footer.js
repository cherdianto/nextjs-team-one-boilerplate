import React from "react";
import { Container, Row, Col, Card } from "react-bootstrap";
import classes from '../styles/Footer.module.css'

const Footer = () => {
  return (
    <div className={classes.footerContainer}>
      <Card text="light" className={`${classes.footer} ${classes.card}`}>
        <Card.Body>
          <Container>
            <Row>
              <Col lg={1}></Col>
              <Col lg={8}>
                  <Row>
                    <Col>Home</Col>
                    <Col>Leaderboards</Col>
                    <Col>Games 1</Col>
                    <Col>Games 2</Col>
                    <Col>Games 3</Col>
                    <Col>Games 4</Col>
                  </Row>
              </Col>
              <Col lg={3}>Lambang Medsos</Col>
            </Row>
          </Container>
          <hr className="bg-dark" />
            <Container>
                <Row>
                    <Col lg={6}>&copy; 2018 Your Games, Inc. All Rights Reserved</Col>
                    <Col></Col>
                    <Col lg={4}>
                        <Row>
                            <Col>Policy</Col>
                            <Col>Services</Col>
                            <Col>Conduct</Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </Card.Body>
      </Card>
    </div>
  );
};

export default Footer;
