import React from 'react';
import { Nav, Navbar } from "react-bootstrap";
import classes from '../styles/Navbar.module.css'
import Link from 'next/link'
import SignedInLinks from './SignedInLinks';
import SignedOutLinks from './SignedOutLinks';
import { connect } from 'react-redux'

const NavbarComponent = (props) => {
    const { authError, user, profile } = props;

    const links = user ? <SignedInLinks /> : <SignedOutLinks />;
  return (
    <Navbar expand="lg" variant="dark" className={classes.navbar} sticky="top">
        <Navbar.Brand className={classes.navbarBrand}><Link href="/"><a>TEAM <strong>O.N.E</strong></a></Link></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className={`${classes.navLink} mr-auto`}>
                <Nav.Link><Link href="/"><a>Home</a></Link></Nav.Link>
                <Nav.Link><Link href="/game-list"><a>Games</a></Link></Nav.Link>
                <Nav.Link><Link href="/game-detail"><a>Game Detail</a></Link></Nav.Link>
                <Nav.Link><Link href="/game-play"><a>Game Play</a></Link></Nav.Link>
            </Nav>
        

            { links }

        </Navbar.Collapse>
      </Navbar>
  )
}

const mapStateToProps = (state) => {
    return {
        authError: state.auth.authError,
        user: state.auth.user,
        profile: state.auth.profile
    }
}

export default connect(mapStateToProps)(NavbarComponent)
