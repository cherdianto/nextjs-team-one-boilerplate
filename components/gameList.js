import React, { Component } from "react";
import { Button, Card, Row, Col } from "react-bootstrap";

class GameListCard extends Component {
  static getInitialProps() {}

  constructor(props) {
    super(props);
  }
  render() {
    const { game_name, game_detail } = this.props;
    return (
      <Card className="container-fluid p-4 text-center">
        {/* <Card.Img variant="top" src={photo} /> */}
        <Card.Body>
          <Card.Title>{game_name}</Card.Title>
          <Card.Text>
            <Row className="my-2">
              <Col>{game_detail}</Col>
            </Row>
          </Card.Text>
          <Button variant="danger">Game Details</Button>
        </Card.Body>
      </Card>
    );
  }
}

export default GameListCard;
