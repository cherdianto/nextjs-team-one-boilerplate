import Footer from '../components/footer'
import NavbarComponent from '../components/navbar'
import Meta from '../components/meta'
import { Container } from 'react-bootstrap'


export default function Layout({ preview, children }) {
  return (
    <>
      <Meta />
      <NavbarComponent />
        <div>
          <main>{children}</main>
        </div>
      <Footer />
    </>
  )
}
