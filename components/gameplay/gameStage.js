import React, { Component, useState } from "react";
import { connect } from "react-redux";
import styles from "../../styles/GameStage.module.css";
import Image from "next/image";
import Router from "next/router";
import { Button } from "react-bootstrap";
import Link from "next/link";
import Modal from "react-modal";
import firebase from '../../services/firebase'

// import { Button } from "react-bootstrap";

class GameStage extends Component {
  state = {
    player: "",
    computer: "",
    winner: "VS",
    classCompPick: "",
    skorPlayer: 0,
    skorComputer: 0,
    onClick: 0,
    gameHistory: null,
    latestScore: null
  };

  componentDidMount() {
    if (this.props.user) {
      this.getUserGameHistoryDB();
    } else {
      console.log("no current user");
      // return history.push('/')
    }
  }

  componentWillUnmount = async () => {
    //update score to database
    

    if(this.props.user){
      const updatedData = [Date.now(),this.state.latestScore]
      const uid = this.props.user.uid
  
      this.setState({
          gameHistory : this.state.gameHistory.push(updatedData)
        }, () => {console.log(this.state.gameHistory)})
  
      try {
        await firebase.database().ref(`users_new/${uid}/gameHistory`).update(this.state.gameHistory)
        console.log('update DB success')
      } catch (error) {
        console.log(error.message)
      }
    }
    else {
      console.log('good bye')
    }
    
  }

  getUserGameHistoryDB = async () => {
          try {
            const userUid = this.props.user.uid;
            const snapshot = await firebase
              .database()
              .ref(`users_new/${userUid}`)
              .once("value");

            let { gameHistory } = snapshot.val();
            let score = snapshot.val().gameHistory;
            let latestScore = 0;

            if (typeof score !== 'undefined'){
              latestScore = gameHistory[gameHistory.length-1][1]
              // console.log(gameHistory)
            } else {
              gameHistory = [Date.now(), 0]
            }

            console.log(`lastest score : ${latestScore}`)
            console.log(gameHistory)

            this.setState({
              latestScore,
              gameHistory
            });

        } catch (error) {
          console.log(error);
        }
    // let { gameHistory } = this.props.profile
    // let score = gameHistory;
    // let latestScore = 0;

    // if (typeof score !== 'undefined'){
    //   latestScore = gameHistory[gameHistory.length-1][1]
    //   console.log(gameHistory)
    // } else {
    //   gameHistory = [Date.now(), 0]
    // }

    // // console.log(`lastest score : ${latestScore}`)
    // // console.log(gameHistory)

    // this.setState({
    //   latestScore,
    //   gameHistory
    // });
  };

  getRandomChoice = () => {
    const items = ["batu", "kertas", "gunting"];
    const item = items[Math.floor(Math.random() * 3)];
    console.log(`komp: ${item}`);
    return item;
  };

  pilihanPlayer = (player) => {
      console.log(`user: ${player}`);
      const computer = this.getRandomChoice();
      let skorPlayer = this.state.skorPlayer;
      let skorComputer = this.state.skorComputer;
      let winner = "";

      //logika game
      if (
        (player === "batu" && computer === "kertas") ||
        (player === "kertas" && computer === "gunting") ||
        (player === "gunting" && computer === "batu")
      ) {
        winner = "Computer Win !";
        skorComputer++;
      } else if (
        (computer === "batu" && player === "kertas") ||
        (computer === "gunting" && player === "batu") ||
        (computer === "kertas" && player === "gunting")
      ) {
        winner = "Player Win ! ";
        skorPlayer++;
      } else {
        winner = "Draw !";
      }

      //Update data ke state
      this.setState((prevState) => {
          return {
            ...prevState,
            player,
            computer,
            winner,
            skorComputer,
            skorPlayer,
            onClick: prevState.onClick + 1
          }
      }, () => {
        console.log(this.state.onClick)
        if(this.state.onClick == 3){
          let newScore = this.state.latestScore

          console.log('permainan selesai')
          if(this.state.skorPlayer > this.state.skorComputer){
            newScore += 10
            console.log('player menang, point +10')
          }
          else if (this.state.skorPlayer == this.state.skorComputer)
            console.log('draw')
          else {
            newScore -= 5
            console.log('player kalah, point -5')  
          }

          this.setState({
            latestScore: newScore,
            onClick : 0,
            skorComputer : 0,
            skorPlayer : 0
          })
        }
          
      })

      
  }

  render () {
   // const [modalIsOpen, setModalIsOpen] = useState(false);

    if (this.props.user) {
      console.log('welcome')
    }
    else{
      // alert('Please login in order to play this game')
      Router.push('/login')
    }

   return (
     <div className={styles.main__battle}>
       <div className={styles.score__board}>
         <h2>Your Score : {this.state.latestScore}</h2>
       </div>
       <div className={styles.score__board}>
         <div id="user-label" className={styles.player__score}>
           user
         </div>
         <span id="user-score">{this.state.skorPlayer}</span> : <span id="computer-score">{this.state.skorComputer}</span>
         <div id="computer-label" className={styles.player__score}>
           comp
         </div>
       </div>
 
       <div id="result" className={styles.result__battle}>
         <p>{this.state.winner} !!!</p>
       </div>
 
       {/* CHOICE */}
 
       <div className={styles.SectionChoice}>
         {/* BUTTON CONTROL */}
         <div className={styles.buttonControl}>
           <Button>
             <Link href="/game-play">RESTART</Link>
           </Button>
 
           {/* <Button onClick={() => setModalIsOpen(true)}>RULES </Button>
          <Modal
            isOpen={modalIsOpen}
            style={{
              overlay: {
                background: "none",
              },
              content: {
                width: "420px",
                height: "280px",
                backgroundColor: "#F6D76D",
                color: "#4C1C12",
                textAlign: "center",
                border: "none",
              },
              button: {
                width: "100px",
                height: "50px",
              },
            }}
          >
            <h2>---- Rules Game ----</h2>
            <h5>🚀 1 round of 3x suits</h5>
            <h5>🚀 If you win, you get 10 points</h5>
            <h5>🚀 if you lose the score is less -5</h5>
            <p>
              ** Klik "LEFT ARROW" to <strong>SAVE</strong> your point **
              <br />
              !!! Klik "RESTART" will <strong>NOT SAVE</strong>not saved your
              point !!!
            </p>

            <div className="modal__rules">
              <button onClick={() => setModalIsOpen(false)}>Close</button>
            </div>
          </Modal> */}
           
           <Button>
             <Link href="/achievements">See Achievments</Link>
           </Button>
         </div>
 
         <div className={styles.choices}>
           <div className={styles.choices__player}>
             <Button id="batu_p" className={styles.batu__player}>
               <Image
                 src="/../public/assets/images/batu.png"
                 alt="batuplayer"
                 width={80}
                 height={80}
                 onClick={() => this.pilihanPlayer("batu")}
               />
             </Button>
 
             <Button id="kertas_p" className={styles.kertas__player}>
               <Image
                 src="/../public/assets/images/kertas.png"
                 alt="kertasplayer"
                 width={80}
                 height={80}
                 onClick={() => this.pilihanPlayer("kertas")}
               />
             </Button>
 
             <Button id="gunting_p" className={styles.gunting__player}>
               <Image
                 src="/../public/assets/images/gunting.png"
                 alt="guntingplayer"
                 width={80}
                 height={80}
                 onClick={() => this.pilihanPlayer("gunting")}
               />
             </Button>
           </div>
 
           <div className={styles.choices__comp}>
             <Button id="batu_c" className={styles.batu__comp}>
               <Image
                 src="/../public/assets/images/batu.png"
                 alt="batucomp"
                 width={80}
                 height={80}
               />
             </Button>
 
             <Button id="kertas_c" className={styles.kertas__comp}>
               <Image
                 src="/../public/assets/images/kertas.png"
                 alt="kertascomp"
                 width={80}
                 height={80}
               />
             </Button>
 
             <Button id="gunting_c" className={styles.gunting__comp}>
               <Image
                 src="/../public/assets/images/gunting.png"
                 alt="guntingcomp"
                 width={80}
                 height={80}
               />
             </Button>
           </div>
         </div>
       </div>
     </div>
   );
 }
};

// DISPATCH
const mapStateToProps = (state) => {
    console.log(state)
    return {
        authError: state.auth.authError,
        user: state.auth.user,
        profile: state.auth.profile
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        signIn: (creds) => dispatch(signIn(creds)),
        getUserData: (uid) => dispatch(getUserData(uid))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(GameStage);
