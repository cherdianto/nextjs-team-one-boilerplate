import React from "react";
import GameStage from "./gameStage";
import HeaderGame from "./headerGame";
import styles from "../../styles/MainGame.module.css";

const MainGame = () => {
  return (
    <div className={styles.main__game}>
      <HeaderGame />
      <GameStage />
    </div>
  );
};

export default MainGame;
