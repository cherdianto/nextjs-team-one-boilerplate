import React from "react";
import styles from "../../styles/HeaderGame.module.css";
import { Button } from "react-bootstrap";
import Link from 'next/link'

const HeaderGame = () => {
  return (
    <div className={styles.header__game}>
      <div className={styles.header__title}>
        <Button className="mr-5 p-30">
          <Link href="/game-detail"> BACK</Link>
        </Button>
        <h1>Rock Paper Scissors</h1>
      </div>
    </div>
  );
};

export default HeaderGame;
