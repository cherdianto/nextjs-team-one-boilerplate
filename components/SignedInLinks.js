import React from 'react';
import { NavLink } from 'react-router-dom';
import { Nav, Navbar, NavDropdown } from "react-bootstrap";
import { connect } from 'react-redux'
import { signOut } from '../redux/actions/authActions'
import Link from 'next/link'
import classes from '../styles/Navbar.module.css'

const SignedInLinks = (props) => {
    const { profile } = props;
    const greeting = 'Hi, '+ profile.firstName;

    return (
        <div>
            <Nav className={`${classes.navLink} ml-auto`}>
                <NavDropdown title={greeting} id="basic-nav-dropdown">
                    <NavDropdown.Item><Link href="/profile"><a>My Profile</a></Link></NavDropdown.Item>
                    <NavDropdown.Item><Link href="/update-profile"><a>Update Profile</a></Link></NavDropdown.Item>
                    <NavDropdown.Item><Link href="/achievements"><a>Your Achievement</a></Link></NavDropdown.Item>
                </NavDropdown>
                <Nav.Link><a onClick={props.signOut}>Logout</a></Nav.Link>
            </Nav> 
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        user: state.auth.user,
        authError: state.auth.authError,
        profile: state.auth.profile
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        signOut: () => dispatch(signOut())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignedInLinks);
